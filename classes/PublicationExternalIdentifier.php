<?php
/**
 * ToDo: doc
 */

/**
 * Class PublicationExternalIdentifier
 */
class PublicationExternalIdentifier {

  const EX_ID_SUBJECT = "RDP_Publication";

  private $external_id,
    $description,
    $order_position;

  /**
   * @var \ExternalID $external_id_object
   */
  private $external_id_object;

  public function __construct() {
    $this->external_id = ExternalID::EMPTY_ID;
    $this->description = "";
    $this->external_id_object = new ExternalID();
  }

  /**
   * @return int
   */
  public function getExternalId() {
    return $this->external_id;
  }

  /**
   * @param int $external_id
   */
  public function setExternalId($external_id) {
    $this->external_id = $external_id;
    $this->external_id_object = ExternalID::fetch($external_id);
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return int
   */
  public function getOrderPosition() {
    return $this->order_position;
  }

  /**
   * @param int $order_position
   */
  public function setOrderPosition($order_position) {
    $this->order_position = $order_position;
  }

  public function save() {
    $id = $this->getExternalId();

    /**
     * If order-position has not been set manually, auto-increment from existing IDs assigned to the publication
     */
    if (empty($this->getOrderPosition())) {

      $exid = ExternalIDRepository::findById($id);
      /**
       * @var \ExternalID $exid
       */
      $publication_id = $exid->getSubjectId();
      $ex_ids = ExternalIDRepository::findBySubjectId(PublicationExternalIdentifier::EX_ID_SUBJECT, $publication_id);
      if (!is_array($ex_ids)) {
        $ex_ids = [$ex_ids];
      }
      $count_ex_id = count($ex_ids);
      $this->setOrderPosition($count_ex_id);
    }

    db_merge(self::table_name())
      ->key(['external_id' => $id])
      ->fields([
        'description' => $this->getDescription(),
        'order_position' => $this->getOrderPosition(),
      ])
      ->execute();
  }

  /**
   * @param int $id
   *
   * @return \PublicationExternalIdentifier
   */
  public static function fetch($id) {
    $result = db_select(self::table_name(), 't')
      ->fields('t')
      ->condition('t.external_id', $id)
      ->execute()
      ->fetch();

    if ($result) {
      $object = new self();
      $object->setExternalId($id);
      $object->setDescription($result->description);
      $object->setOrderPosition($result->order_position);
      return $object;
    }
    else {
      return new self();
    }
  }

  /**
   * @param int $order_position
   *
   * @return \PublicationExternalIdentifier
   */
  public static function fetchByOrderPosition($order_position, $pub_id) {
    $query = db_select(self::table_name(), 't');
    $query->leftJoin('rdp_external_id', 'r', 't.external_id = r.id');
    $result = $query->fields('t')
      ->fields('r')
      ->condition('t.order_position', $order_position)
      ->condition('r.subject_id', $pub_id)
      ->execute()
      ->fetch();

    if ($result) {
      $object = new self();
      $object->setExternalId($result->external_id);
      $object->setDescription($result->description);
      $object->setOrderPosition($result->order_position);
      return $object;
    }
    else {
      return new self();
    }
  }

  /**
   * @param $publication_id
   *
   * @return \ExternalID[]
   */
  public static function fetchAllByPublicationId($publication_id) {
    $list = ExternalIDRepository::findBySubjectId(self::EX_ID_SUBJECT,
      $publication_id);
    return $list;
  }

  /**
   * @param int $publication_id
   * @param bool $display_heading Toggles block heading
   * @param bool $display_editor_actions Toggles "edit" and "delete" action icons
   *
   * @return string HTML markup
   */
  public static function display_block($publication_id, $display_heading = TRUE, $display_editor_actions = TRUE) {

    /**
     * fetch Publication object
     */
    $publication = PublicationRepository::findById($publication_id);

    /**
     * Prepare heading
     */
    $icon_path = $GLOBALS['base_url'] . '/'
      . drupal_get_path('module', 'sfb_literature')
      . '/resources/sharing-linking-icon.svg';
    $icon = '<img src="' . $icon_path . '" alt="" class="img-rounded" style="width: 32px; max-width: 32px;">';
    if($display_heading) {
      $heading = '<h4>' . $icon . '&nbsp;External Resources</h4>';
    } else {
      $heading = '';
    }

    /**
     * Provide "Add identifier" link if user has permission
     */
    if ($display_editor_actions && $publication->check_permission(Publication::PERMISSION_EDIT)) {
      $add_link = l('<span class="glyphicon glyphicon-plus"></span> Add External Resource',
        $publication->url("add_external_id"),
        [
          'html' => TRUE,
          'attributes' => [
            'title' => 'Add External Identifier',
            'class' => 'btn btn-xs btn-primary',
            'style' => 'margin-top: 8px;',
            //'style' => 'float: right; margin: 6px;',
          ],
        ]);
    }
    else {
      $add_link = '';
    }

    /**
     * Prepare and add CSS styles
     */
    $css_style = /** @lang CSS */
      'p.rdp-ex-id-list-item {
        background-color: #f9f9f9; 
        padding: 4px; 
        border-radius: 4px; 
        margin: 0 0 4px;
      }
      p.rdp-ex-id-list-item:hover {
        background-color: #f5f5f5; 
      }';
    drupal_add_css($css_style, 'inline');

    /**
     * Assemble list of Ex-IDs
     */
    $list = self::fetchAllByPublicationId($publication_id);
    $display_list = "";
    if ($list and !is_array($list)){
      $list = [$list];
    }
    if ($list and $count = count($list)) {

      // Check if there is at least one OrderPosition on NULL,
      //in that case, reset ordering
      foreach ($list as $element) {
        $order_position = self::fetch($element->getId())->getOrderPosition();
        if ($order_position == NULL or $order_position > $count) {
          $counter = 1;
          foreach ($list as $ext_id) {
            $ext_pub_id = self::fetch($ext_id->getId());
            $ext_pub_id->setOrderPosition($counter);
            $ext_pub_id->save();
            $counter++;
          }
          break;
        }
      }


      if ($count > 1) {
        $list_sorted = [];
        for ($i = 1; $i<=count($list);$i++) {
          $external_id = ExternalIDRepository::findById(self::fetchByOrderPosition($i, $publication_id )->getExternalId());
          $list_sorted [] = $external_id;
        }
        $list = $list_sorted;
      }


      /**
       * jQuery script to enable "copy to clipboard" links.
       */
      $copy_to_clipboard_javascript = /** @lang javascript */
        <<<SCRIPT
jQuery(document).ready(function(){
	jQuery("a[name=copy_pre]").click(function() {
		var id = jQuery(this).attr('id'); 
		var el = document.getElementById(id);
		var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = el.textContent;
    document.body.appendChild(tempInput);
    tempInput.select();
		document.execCommand('copy');
		return false;
	});
});
SCRIPT;
      drupal_add_js($copy_to_clipboard_javascript, 'inline');

      foreach ($list as $item) {
        $pre_action = '';
        if (!($pub_ex_id = self::fetch($item->getId()))->isEmpty()) {

          if ($display_editor_actions && $publication->check_permission(Publication::PERMISSION_EDIT)) {
            $up = "&nbsp;" . l("<span class='glyphicon glyphicon-menu-up'></span>",
                $pub_ex_id->url("up"), [
                  'html' => TRUE,
                  'attributes' => [
                    'style' => 'font-size: 80%;',
                    'title' => 'Move Up',
                  ],
                ]) . "&nbsp;";
            $down = "&nbsp;" . l("<span class='glyphicon glyphicon-menu-down'></span>",
                $pub_ex_id->url("down"), [
                  'html' => TRUE,
                  'attributes' => [
                    'style' => 'font-size: 80%;',
                    'title' => 'Move Down',
                  ],
                ]) . "&nbsp;";
            $pre_action = $up.$down;
          }


          /**
           * Generate arbitrary but unique id for CSS elements to enable copy-to-clipboard functionality
           */
          $css_element_id = substr(md5($pub_ex_id->getExternalId()), 0, 12);

          /**
           * Add the "copy to clipboard" icon
           */
          $clipboard_icon = '<span id="' . $css_element_id . '" class="hidden">'
            . $item->url("resolve") . '</span>
            <a href="" id="' . $css_element_id . '" name="copy_pre" title="Copy to clipboard">
            <span class="glyphicon glyphicon-copy"></span></a>&nbsp;';
          $actions = $clipboard_icon;

          if ($display_editor_actions && $publication->check_permission(Publication::PERMISSION_EDIT)) {
            $actions .= l("<span class='glyphicon glyphicon-edit'></span>",
              $pub_ex_id->url("edit"), [
                'html' => TRUE,
                'attributes' => [
                  'style' => 'font-size: 80%;',
                  'title' => 'Edit External Identifier',
                ],
              ]);
            $actions .= "&nbsp;" . l("<span class='glyphicon glyphicon-trash'></span>",
              $pub_ex_id->url("delete"), [
                'html' => TRUE,
                'attributes' => [
                  'style' => 'font-size: 80%;',
                  'title' => 'Delete External Identifier',
                  'onclick' => 'if(!confirm("Delete the Externale Identifier?")){return false;}',
                ],
              ]) . "&nbsp;";
          }

          $display_list .= '<a id="ext_ids"></a>'."<p class=\"rdp-ex-id-list-item\">" .$pre_action. ExternalIDRenderer::badge($item)
            . $actions
            . $pub_ex_id->getDescription()
            . "</p>";
        }
      }
    }
    else {
      /**
       * Do not display heading is no items to display
       * and user is not permitted to add items
       */
      if (!$publication->check_permission(Publication::PERMISSION_EDIT)) {
        $heading = '';
      }

    }

    /**
     * Assemble the entire block
     */
    $block_external_identifiers = '<div style="margin-top: 20px; margin-bottom: 40px;">'
      . $heading . $display_list . $add_link .
      '</div>';

    return $block_external_identifiers;
  }

  public function isEmpty() {
    return $this->external_id == ExternalID::EMPTY_ID ? TRUE : FALSE;
  }

  public function url($type) {
    $publication_id = $this->external_id_object->getSubjectId();
    switch ($type) {
      case 'delete':
        $url = Publication::url_by_id($publication_id) . "/delete-external-id/" . $this->getExternalId();
        return $url;
        break;
      case 'up':
        $url = Publication::url_by_id($publication_id) . "/order-external-id/" . $this->getExternalId(). '/up/';
        return $url;
        break;
      case 'down':
        $url = Publication::url_by_id($publication_id) . "/order-external-id/" . $this->getExternalId(). '/down/';
        return $url;
        break;
      case 'edit':
        $url = Publication::url_by_id($publication_id) . "/edit-external-id/" . $this->getExternalId();
        return $url;
        break;
      default:
        break;
    }
    return '';
  }

  /**
   * Deletes the instance and corresponding @see \ExternalID instance from
   * repositories.
   *
   * @return bool TRUE on successful deletion.
   */
  public function delete() {
    $TXN = db_transaction();
    if (ExternalIDRepository::delete($this->external_id)) {

      try {
        db_delete(self::table_name())
          ->condition('external_id', $this->getExternalId())
          ->execute();

      } catch (Exception $exception) {
        watchdog(__CLASS__, $exception->getMessage(), WATCHDOG_ERROR);
        $TXN->rollback();
        return FALSE;
      }
      $this->external_id_object = new ExternalID();
      $this->external_id = ExternalID::EMPTY_ID;
      $this->description = "";
      return TRUE;
    }
  }

  private static function table_name() {
    return 'rdp_publication_externalid';
  }



  public function getForm($form_type = 'create') {
    $form = [];

    $form['exid_context'] = [
      '#type' => 'hidden',
      '#value' => PublicationExternalIdentifier::EX_ID_SUBJECT,
    ];

    $types = ExternalIDTypeRepository::findAll();
    /** @var \ExternalIDType[] $types */
    $options = [];
    foreach ($types as $type) {
      $options[$type->getId()] = $type->getLabel();
    }

    if ($form_type == 'create') {
      /**
       * Retrieve user last-selected ID-Type from Drupal cache
       */
      global $user;
      $cache_id = "rdp_publication_exid_type_select_default_" . $user->uid;
      if ($cache = cache_get($cache_id)) {
        $exid_type_default_value = $cache->data;
      }
      else {
        $exid_type_default_value = NULL;
      }
    }
    else {
      $exid_type_default_value = $this->external_id_object->getType();
    }
    asort($options);
    $form['exid_type'] = [
      '#type' => 'select',
      '#title' => 'External ID Type',
      '#options' => $options,
      '#default_value' => $exid_type_default_value,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => 'sfb_literature_add_id_type_switch_ajax_callback',
        'wrapper' => 'external-id-type-switch-ajax-wrapper',
        'progress' => ['type' => 'none'],
      ],
    ];

    $form['resolver_markup'] = [
      '#markup' => self::render_external_id_type_info($exid_type_default_value),
      '#prefix' => '<div id="external-id-type-switch-ajax-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['exid_value'] = [
      '#type' => 'textfield',
      '#title' => 'Value',
      '#description' => t('Value of the identifier without preceding resolver URL. Maximum 255 characters allowed.'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => 'Description',
      '#maxlength' => 2000,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => "Add Identifier",
    ];

    $form['cancel'] = [
      '#type' => 'submit',
      '#value' => t("Cancel"),
      '#submit' => ['sfb_literature_add_id_cancel'],
      '#limit_validation_errors' => [],
    ];

    if ($form_type == 'edit') {
      $form['exid_type']['#disabled'] = TRUE;
      $form['exid_value']['#default_value'] = $this->external_id_object->getValue();
      $form['description']['#default_value'] = $this->getDescription();
      $form['submit']['#value'] = 'Save Changes';
      $form['cancel']['#submit'] = ['sfb_literature_edit_external_id_cancel'];
    }

    return $form;
  }


  /**
   * Helper function
   */
  public static function render_external_id_type_info($exid_type_id) {
    if ($exid_type_id) {
      $type = ExternalIDTypeRepository::findById($exid_type_id);
      /**
       * @var \ExternalIDType $type
       */
      $resolver = $type->getResolveUrl();
      $text = "ID Resolver:&nbsp;";
      $text .= !empty($resolver) ? "<code>" . $resolver . "</code>" : "<strong>None</strong>. Please insert fully qualified URL.";
      $text = '<div class="well well-sm text-info">' . $text . '</div>';
    }
    else {
      $text = '';
    }
    return $text;
  }
}
