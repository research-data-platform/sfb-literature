<?php

/**
 * @file This file contains forms and handlers for the entry page and
 *   overviewing existing publication entries.
 */

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('inc', 'sfb_literature', 'includes/article_def');

define('VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING', 'ASC');
define('VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING', 'DESC');

define('SFB_LITERATURE_URL_PAGE_DEFAULT', 'literature');

drupal_set_title('Published Data Registry');

function getOrdering($orderings, $defaultOrdering) {
  $sort = NULL;

  if (isset($_REQUEST['sort'])) {
    $sort = $_REQUEST['sort'];
  }

  // E.g.: PATTERN '-c' --> ORDER BY creation_time DESC
  $pattern = '/^(-?)([' . implode('', array_keys($orderings)) . '])$/i';

  if (preg_match($pattern, $sort, $match)) {
    $d = $match[1] == '-' ? VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING : VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING;
    $k = $orderings[$match[2]];
    $l = $match[2];

    return ['column' => $k, 'order' => $d, 'letter' => $l];
  }

  return $defaultOrdering;
}

function invertOrder($order) {
  if ($order == VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING) {
    return VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING;
  }

  return VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING;
}

function defaultInverseNoneOrder($letter, $default, $existingOrder) {
  if ($existingOrder['letter'] == $letter) {
    $o = $existingOrder['order'];
    $inv = invertOrder($default);

    if ($o == $default) {
      return $inv;
    }

    return NULL;
  }

  return $default;
}

function mapOrderingString($o, $map) {
  if (!array_key_exists($o, $map)) {
    return NULL;
  }

  return $map[$o];
}

function orderingToPrefix($o) {
  $map = [
    VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING => '',
    VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING => '-',
    NULL => '',
  ];

  return mapOrderingString($o, $map);
}

/**
 * Formats a link for changing the ordering of a columns. Ordering oscilates
 * between DEFAULT, INVERSE OF DEFAULT, and NULL.
 */
function makeTitleOrderLink($title, $sortLetter, $defaultOrdering, $existingOrder) {
  $o = defaultInverseNoneOrder($sortLetter, $defaultOrdering, $existingOrder);

  // Characters:
  //  - U+25B4: BLACK UP-POINTING SMALL TRIANGLE
  //  - U+25BE: BLACK DOWN-POINTING SMALL TRIANGLE

  $os = [
    VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING => "&#x25B4;",
    VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING => "&#x25BE;",
    NULL => '',
  ];

  $k = NULL;

  if ($existingOrder['letter'] == $sortLetter) {
    $k = $existingOrder['order'];
  }

  $symbol = $k !== NULL ? ' ' . mapOrderingString($k, $os) : '';

  //get current search query if existing
  if (isset($_GET['search_query'])) {
    $search_query = "&search_query=" . $_GET['search_query'];
  }
  else {
    $search_query = "";
  }
  if ($o !== NULL) {

    return sprintf('<a href="?sort=%s%s%s">%s%s</a>', orderingToPrefix($o), StringEscapeHtml($sortLetter),
      $search_query, StringEscapeHtml($title), $symbol);
  }

  return sprintf('<a href="?sort=%s">%s%s</a>', $search_query, StringEscapeHtml($title), $symbol);
}

function GetDoiPmidHtml($doi, $pmid) {
  if (IsNullOrWhitespace($doi) && IsNullOrWhitespace($pmid)) {
    return '';
  }

  $sdoi = !IsNullOrWhitespace($doi) ? Publication::formatDOILink($doi) : '';
  $spmid = !IsNullOrWhitespace($pmid) ? Publication::formatPMIDLink($pmid) : '';

  $code = '<div style="line-height: 1.1;display: inline-block;">%s<br><div style="border-top: 1px dotted #ccc; margin-top: 3px; padding-top: 3px;">%s</div></div>';

  return sprintf($code, $sdoi, $spmid);
}

/**
 * Implements Landing Page (P1.View all publications)
 */
function sfb_literature_main() {

  // Get assigned Working-Groups of the current user

  $user = User::getCurrent();
  $user_wgs = $user->getUserWorkingGroups(TRUE);

  // Get ordering of rows
  $defaultOrdering = [
    'column' => 'publication_year',
    'order' => 'DESC',
    'letter' => 'c',
  ]; // Default: Newest entry is on top
  $orderings = [
    't' => 'title',
    'a' => 'first_author',
    'j' => 'journal_title',
    'y' => 'publication_year',
    'c' => 'creation_time',
    'g' => 'wg',
  ];
  $ordering = getOrdering($orderings, $defaultOrdering);

  $result = NULL;

  if (isset($_GET['show_all']) or
    isset($_GET['linkings']) or
    isset($_GET['wg'])or
    isset($_GET['ext_id']) or
    isset($_GET['num_wgs']) or
    isset($_GET['pubtype'])) {
    $use_pager = FALSE;
  }
  else {
    $use_pager = TRUE;
  }

  $result = PublicationRepository::findAllAndOrderByDefault($ordering, $use_pager);
  $result_wg = -1;
  $result_ext_id = -1;
  $result_num_wgs = -1;
  $result_linkings = -1;
  $result_pubtypes = -1;

  // check for filtering
  // check if filtered by wg
  if (isset($_GET['wg'])) {
    $result_wg = [];
    // change result to all publications with the given working group
    $wg_id = $_GET['wg'];
    $wg = WorkingGroupRepository::findById($wg_id);
    $wg_name = $wg->getShortName();
    $db_condition = db_or();
    $db_condition->condition('wg', '%' . db_like($wg_name) . '%', 'LIKE');
    $result_wg = PublicationRepository::findBy($db_condition, $ordering);
    $result = $result_wg;
  }

  // check if filtered by external id
  if (isset($_GET['ext_id'])) {
    $result_ext_id = [];
    // change result to all publication with the given external id
    $ext_id_type_id = (int) $_GET['ext_id'];
    $ext_ids = ExternalIDRepository::findByTypeSubject($ext_id_type_id, PublicationExternalIdentifier::EX_ID_SUBJECT);
    if ($ext_ids) {
      if (!is_array($ext_ids)) {
        $ext_ids = [$ext_ids];
      }
      $result_ext_id = [];
      foreach ($ext_ids as $id) {
        $subject_id = (int) $id->getSubjectId();

        if (PublicationRepository::findById($subject_id)) {
          $result_ext_id [$subject_id] = PublicationRepository::findByIdAndOrder($subject_id, $ordering);
        }
      }
    }
    $result = $result_ext_id;
  }
  // check if filtered by number of working groups
  if (isset($_GET['num_wgs'])) {
    $result_num_wgs = [];
    // change result to all publication with the given working group count
    $num_wgs = (int) $_GET['num_wgs'];
    $all_pubs = PublicationRepository::findAll();
    foreach ($all_pubs as $pub) {
    $wg_string = $pub->wg;
    $wg_arr = explode(',', $wg_string);
    $wg_count = count($wg_arr);

    if ($wg_count == $num_wgs) {
      $result_num_wgs [] = $pub;
    }
  }
  $result = $result_num_wgs;
}

  // check if filtered by linked objects
  if (isset($_GET['linkings'])) {
    $result_linkings = [];
    $rdp_linking = RDPLinking::getLinkingByName('sfb_literature',
      'pubreg_articles');
    switch ($_GET['linkings']) {
      case 'cellmodels':
        $linking_string = 'cellmodel_cell_line';
        if (db_table_exists('cellmodel_cell_line')) {
          $ids = CellLineRepository::getAllIds();
        }
        else {
          $ids = [];
        }
        break;
      case 'antibodies':
        $linking_string = 'antibody_antibody2';
        if (db_table_exists('antibody_antibody2')) {
          $ids= AntibodiesRepository::getAllIds();
        }
        else {
          $ids = [];
        }
        break;
      case 'mouselines':
        $linking_string = 'mouselines_mouseline';
        if (db_table_exists('mouselines_mouseline')) {
          $ids= MouselineRepository::getAllIds();
        }
        else {
          $ids = [];
        }
        break;
      case 'notebooks':
        $linking_string = 'lab_notebooks';
        break;
      default:
        $linking_string = '';
        $ids = [];
    }

    if ($linking_string == 'lab_notebooks' and db_table_exists('linking_pubreg_labnotebook')) {
      $results = db_select('linking_pubreg_labnotebook', 'lpl')
        ->fields('lpl', ['pubreg_article'])
        ->execute();
      $publication_ids = [];
      foreach($results as $result) {
        $publication_ids[] = $result->pubreg_article;
      }
      foreach($publication_ids as $publication_id){
        $publication = PublicationRepository::findByIdAndOrder($publication_id, $ordering);
        if (!in_array($publication, $result_linkings)) {
          $result_linkings [] = $publication;
        }
      }
    } else {

      foreach ($ids as $id) {
        $publication_ids = $rdp_linking->getLinkings($linking_string, $id);
        foreach($publication_ids as $publication_id){
          $publication = PublicationRepository::findByIdAndOrder($publication_id, $ordering);
          if (!in_array($publication, $result_linkings)) {
            $result_linkings [] = $publication;
          }
        }
      }
    }
    $result = $result_linkings;
  }

  //check if filtered by pubtype
  if (isset($_GET['pubtype'])) {
    $result_pubtypes = [];
    $result_pubtype = $_GET['pubtype'];
    //change result to all publications with given publication type
    $all_pubs = PublicationRepository::findAll();
    foreach ($all_pubs as $pub) {
      $pubtype_string = $pub->publication_type;

      if ($pubtype_string == $result_pubtype) {
        #drupal_set_message('<pre>'.print_r($pub,true).'</pre>');
        $result_pubtypes [] = $pub;
      }
    }
    #drupal_set_message('<pre>'.print_r($result_pubtypes,true).'</pre>');
    $result = $result_pubtypes;
  }

  $arr_all = [$result_wg, $result_ext_id, $result_num_wgs, $result_linkings, $result_pubtypes];
  $arr_intersect = [];
  foreach ($arr_all as $element) {
    if (is_array($element)) {
      $arr_part_element = [];
      foreach ($element as $part_element) {
        $arr_part_element [] = $part_element->id;
      }
      $arr_intersect [] = $arr_part_element;
    }
  }

  $result_ids = -1;
  if (count($arr_intersect) > 1) {
    $result_ids = call_user_func_array('array_intersect', $arr_intersect);
  }

  if ($result_ids !== -1) {
    $result_intersect = [];
    foreach ($result_ids as $id) {
      $result_intersect [$id] = PublicationRepository::findByIdAndOrder($id, $ordering);
    }
    $result = $result_intersect;
  }

  $count_publications = count($result);

  module_load_include('inc', 'sfb_literature', 'includes/session');
  if ($result_wg !== -1 or $result_ext_id !== -1 or $result_num_wgs !== -1) {
    if (empty($result)) {
      lists_session("publications_filter", -2);
      $export_btn_str = ' Excel-Export (Empty)';
      $export_ris_btn_str = ' RIS-Export (Empty)';
      $export_json_btn_str = ' JSON-Export (Empty)';
    }
    else {
      lists_session("publications_filter", $result);
      $export_btn_str = ' Excel-Export (Filtered)';
      $export_ris_btn_str = ' RIS-Export (Filtered)';
      $export_json_btn_str = ' JSON-Export (Filtered)';
    }
  }
  else {
    lists_session("publications_filter", -1);
    $export_btn_str = ' Excel-Export (All)';
    $export_ris_btn_str = ' RIS-Export (All)';
    $export_json_btn_str = ' JSON-Export (All)';
  }

  // Construct table-rows
  $rows = [];
  if ($result) {
    foreach ($result as $row) {
      // Make title link
      $link = url(sprintf('literature/publications/%s', StringEscapeHtml($row->id)));
      $linkpubtitle = sprintf('<a href="%s" style="color: #777; font-style: italic; letter-spacing: 0.45pt;">%s</a>',
        $link, t('Unnamed publication'));

      if (!IsNullOrWhitespace($row->title)) {
        $linkpubtitle = sprintf('<a href="%s">%s</a>', $link, StringEscapeHtml($row->title));
      }
      //add open access symbol to title
      $open_access_symbol = '';
      if ($row->open_access == 1) {
        $open_access_symbol = '<img src="' . base_path()
          . drupal_get_path('module', 'sfb_literature')
          . '/resources/open_access.svg"
        height="24px" alt="(Open Access)" data-toggle="tooltip" title="Open Access"
        style="vertical-align:center; margin: 5px;" />
      ';
      }

      // get working group icon
      //TODO: wenn die Funktion wg_get_names an die Funktion aus Antibody-Module angepasst wird, dann muss auch folgender Code angepasst werden...
      $working_group_name = 'ag_unknown';
      if (!empty($row->wg)) {
        $working_group_name = $row->wg;
        $working_group_names = wg_get_names();
        if (isset($working_group_names[$working_group_name])) {
          $working_group_name = $working_group_names[$working_group_name];
        }
      }
      else {
        $row->wg = 'ag_unknown';
      }

      $public_icon_path = WorkingGroupRepository::findByShortName($row->wg)
        ->getIconPath(WorkingGroup::ICON_SIZE_SMALL);
      $icon_element = [
        'path' => $public_icon_path,
        'title' => $working_group_name,
        'attributes' => [],
      ];

      // Row content
      $rows[] = [
        //['data' => $public_icon, 'class' => ['text-center']],
        $linkpubtitle,
        $open_access_symbol,
        Publication::renderShortAuthorList($row->authors),
        StringEscapeHtml($row->journal_title),
        StringEscapeHtml($row->publication_year),
        GetDoiPmidHtml($row->doi, $row->pmid),
        formatEditLink($row->wg, $user_wgs, $row->id),
      ];
    }
  }

  // Format Table
  // 'Title', 'Authors', 'Journal', 'Publication year', 'DOI', ''
  $header = [];
  /*$header[] = [
    'data' => makeTitleOrderLink('AG', 'g', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering),
    'style' => ['min-width:50px;'],
    'class' => ['text-center'],
  ];*/
  $header[] = makeTitleOrderLink('Title', 't', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering);
  $header[] = ''; // Open Access
  $header[] = makeTitleOrderLink('Authors', 'a', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering);
  $header[] = makeTitleOrderLink('Journal', 'j', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering);
  $header[] = makeTitleOrderLink('Publication year', 'y', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING, $ordering);
  $header[] = '<div style="line-height: 1.1;display: inline-block; vertical-align: bottom;">DOI<br><div style="border-top: 1px dotted #ddd; margin-top: 3px; padding-top: 2px;">PMID</div></div>';
  $header[] = '';

  //print table
  if ($count_publications > 0) {
    $pagecontent = theme('table', ['header' => $header, 'rows' => $rows]);
    if (!arg(1)) {
      $pagecontent .= theme('pager');
    }
  }
  else {
    $pagecontent = '<h4>No publications found.</h4><div> </div>';
  }

  /**
   * Search and filtering block
   */
  // filter options
  $filter_form = drupal_get_form('sfb_literature_form_filter', $result_wg, $result_ext_id, $result_num_wgs, $result_linkings, $result_pubtypes);
  $search_filter_block = drupal_render($filter_form);

  $btn_options = [
    'html' => TRUE,
    'attributes' => ['class' => ['btn btn-default btn-sm']],
  ];
  $btn_export_ris = l('<span class="glyphicon glyphicon-download-alt"></span>' . $export_ris_btn_str,
    SFB_LITERATURE_PAGE_EXPORT_MISC . 'ris', $btn_options);
  $btn_export_json = l('<span class="glyphicon glyphicon-download-alt"></span>' . $export_json_btn_str,
    SFB_LITERATURE_PAGE_EXPORT_JSON, $btn_options);
  $btn_export_excel = l('<span class="glyphicon glyphicon-download-alt"></span>' . $export_btn_str,
    SFB_LITERATURE_PAGE_EXPORT, $btn_options);
  $btn_statistics = l('<span class="glyphicon glyphicon-stats"></span> Publication Statistics',
    SFB_LITERATURE_PAGE_STATISTICS, $btn_options);

  // 'Create new publication'-Button for people with create access
  if (user_access(SFB_LITERATURE_PERMISSION_CREATE)) {
    $btn_add_publication = l('<span class="glyphicon glyphicon-plus"></span> Create New Publication',
      SFB_LITERATURE_PAGE_CREATE,
      [
        'html' => TRUE,
        'attributes' => ['class' => ['btn btn-primary btn-sm']],
      ]);
  }
  else {
    $btn_add_publication = '';
  }

  $navbar_actions = '
    <nav class="navbar">
      <div class="container-fluid">
       <div class="navbar-header navbar-default">
          <button type="button" class="navbar-default navbar-toggle" data-toggle="collapse" data-target="#actionNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="actionNavbar">
          <ul class="nav navbar-nav navbar-left">
            <li>' . $btn_add_publication . '</li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li>' . $btn_export_ris . '</li>
            <li>' . $btn_export_json . '</li>
            <li>' . $btn_export_excel . '</li>
            <li>' . $btn_statistics . '</li>
          </ul>
        </div>
      </div>
    </nav>
';

  // show all button
  $btn_show_all_publications = l('<span class="glyphicon glyphicon-zoom-in"></span> show all',
    SFB_LITERATURE_PAGE_MAIN, [
      'html' => TRUE,
      'query' => ['show_all' => 'yes'],
      'attributes' => [
        'data-toggle' => 'tooltip',
        'title' => "Show All Publications",
        'class' => ['btn btn-primary btn-xs'],
        'style' => 'position: absolute; right: 5px; margin: 10px;',
      ],
    ]);

  $pagecontent = $navbar_actions . $search_filter_block . $btn_show_all_publications . $pagecontent . $btn_add_publication;

  return $pagecontent;
}
