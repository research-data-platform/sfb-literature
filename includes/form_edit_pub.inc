<?php

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('inc', 'sfb_literature', 'includes/article_def');

/**
 * @file This file contains forms and handlers for editing specific publication
 *   entries.
 */

/**
 * Implements edit publication (P4.Edit publication)
 */
function sfb_literature_edit($form, &$form_state, $publication_id) {
  $user = User::getCurrent();
  $user_wgs = $user->getUserWorkingGroups(TRUE);
  $r = fetchPubAndCheck($publication_id, $user_wgs);

  // Needed due to drupal's overall perfectness
  if (!$r) {
    module_invoke_all('exit');
    exit();
  }

  $fields = Publication::get_form_fields();
  $names = Publication::get_field_names();
  $form = [];

  $set = createTextSet($form, 'Fields', t('Enter the properties in fields below.'), 'pubdata');
  $form['pubid'] = ['#type' => 'hidden', '#value' => $publication_id];


  foreach ($fields as $k => $type) {
    if ($k == 'wg') {
      $form[$set][$k] = get_form_field_working_group();
    }
    else {
      if ($k == 'publication_type') {
        $form[$set][$k] = get_form_field_publication_type();
      }
      else {
        if ($k == 'open_access') {
          $form[$set][$k] = get_form_field_open_access();
        }
        else {
          if ($k == 'subproject') {
            $form[$set][$k] = get_form_field_subproject();
          } else {
            $form[$set][$k] = [
              '#type' => 'textfield',
              '#title' => t($names[$k]),
              '#maxlength' => NULL,
            ];
          }
        }
      }
    }
  }

  foreach ($fields as $k => $type) {
    if ($k !== 'wg' and $k !== 'subproject') {
      $form[$set][$k]['#default_value'] = $r->$k; // $k not k!
    }
    elseif ($k == 'wg') {
      $default = explode(', ',$r->$k);
      $form[$set][$k]['#default_value'] = $default;
    }
    elseif ($k == 'subproject') {
      $default = explode(', ',$r->$k);
      $form[$set][$k]['#default_value'] = $default;
    }
  }

  $form['external_resources'] = [
    '#type' => 'fieldset',
    '#title' => t('External Resources'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['external_resources']['resources'] = [
    '#markup' => PublicationExternalIdentifier::display_block($publication_id, FALSE),
  ];

  /**
   * Get linking antibodies edit field if linking exists.
   */
  $antibody_linking = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');
  if ($antibody_linking) {
    $form['linked_antibodies'] = [
      '#type' => 'fieldset',
      '#title' => t('Linked Antibodies'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['linked_antibodies']['antibodies'] = $antibody_linking->getEditForm('pubreg_articles', $publication_id);
  }

  /**
   * Get Labnotebooks edit field of module is enabled.
   */
  if (module_exists("sfb_labnotebooks")) {
    // get ids of all already linked lab notebooks
    $linked_labnotebooks = sfb_labnotebooks_linking_get_items_by_publication($publication_id);
    $linked_labnotebooks_ids = [];
    foreach ($linked_labnotebooks as $ln) {
      $linked_labnotebooks_ids[] = $ln['id'];
    }

    // get lab notebook linking field (from lab notebook module) and add it to the form
    $form['linked_labnotebooks'] = sfb_labnotebooks_linking_edit_items([
      'fieldset' => TRUE,
      'items_list' => $linked_labnotebooks_ids,
    ]);
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  $form['cancel'] = [
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => ['sfb_literature_edit_cancel'],
  ];

  return $form;
}

/**
 * Create-Action validation function for @see sfb_literature_edit().
 */
function sfb_literature_edit_validate($form, &$form_state) {

  $user = User::getCurrent();
  $user_wgs = $user->getUserWorkingGroups(TRUE);

  $pub_type = $form_state['values']['publication_type'];
  //check if publication type is empty, when no type is set
  if (empty($pub_type)) {
    form_set_error('publication_type', 'Please select a publication type for the publication.');
  }

  $wg = $form_state['values']['wg'];
  //check if working group field is empty by cylcing through each wg option
  $set_weg = false;
  foreach ($wg as $w) {
    if ($w !== 0){
      $set_weg = true;
    }
  }

  if (!$set_weg and !user_access('administer site configuration')) {
    form_set_error('wg', 'Please select at least one working group for the publication.');
  }

  // Check for numbers
  $fields = Publication::get_form_fields();
  $names = Publication::get_field_names();

  $r = formCheckFieldValueTypes($fields, $names, $form_state['values']);

  if ($r !== TRUE) {
    drupal_set_message(sprintf('The field %s is missing!', $r), 'error');
  }

  if (fetchPubAndCheck($form_state['values']['pubid'], $user_wgs) === FALSE) {
    form_set_error('pubid', t('Publication Error'));
    drupal_access_denied();
  }
}

/**
 * Exract publication ID and protect for XSS.
 */
function getpubchecked(&$form_state) {
  $publication = $form_state['values']['pubid'];

  // Protect against injections / xss
  if (!preg_match('/^\s*\d+\s*$/', $publication)) {
    drupal_access_denied();
    return NULL;
  }

  return $publication;
}

/**
 * Cancel handler for @see sfb_literature_edit().
 */
function sfb_literature_edit_cancel($form, &$form_state) {
  $publication = getpubchecked($form_state);

  if ($publication === NULL) {
    return;
  }

  $form_state['redirect'] = sprintf('literature/publications/%s/', StringEscapeHtml($publication));
}

/**
 * Submit handler for @see sfb_literature_edit().
 */
function sfb_literature_edit_submit($form, &$form_state) {

  $publication = getpubchecked($form_state);

  if ($publication === NULL) {
    return;
  }

  //drupal_set_message('Pub: ' . $publication);
  $fields = Publication::get_form_fields();

  $data = [];

  $data['wg'] = $form_state['values']['wg'];
  $data['open_access'] = $form_state['values']['open_access'];
  $data['publication_type'] = $form_state['values']['publication_type'];

  foreach ($fields as $k => $t) {

    $v = '';
    if ($k == 'wg') {
      foreach ($form_state['values']['wg'] as $wg){
        if ($wg !== 0){
          $v .= $wg.', ';
        }

      }
      $v = substr($v,0,-2);

    }
    else if ($k == 'subproject') {
      foreach ($form_state['values']['subproject'] as $sp){
        if ($sp !== 0){
          $v .= $sp.', ';
        }

      }
      $v = substr($v,0,-2);
    }
    else {
      $v = $form_state['values'][$k];
    }

    // Handle empty values
    if (!IsNullOrWhitespace($v)) {
      $data[$k] = trim($v);
    }
    else {
      if ($t == Publication::FORM_FIELD_TYPE_INTEGER) {
        $data[$k] = NULL;
      }
      else {
        $data[$k] = '';
      }
    }
  }

  // Perform update if there are changes
  if (count($data) > 0) {
    PublicationRepository::save($data, $publication, $form_state);
    $form_state['redirect'] = sprintf('literature/publications/%s/', StringEscapeHtml($publication));
  }
}
