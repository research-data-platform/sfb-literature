<?php

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('inc', 'sfb_literature', 'includes/article_def');

define('PHP_EXCEL_PHP_PATH_REL', '../PHPExcel/Classes/PHPExcel.php');


/**
 * @file The literature export components.
 */

/**
 * The abstract base class for LiteratureExporters.
 */
abstract class LiteratureExporter
{
    protected function __construct()
    {
    }

    /**
     * The abstract base for Export-Impelementations.
     *
     * @param $literature The literature to export.
     */
    abstract protected function DoExport($literature);

    /**
     * The abstract base for Export instance-names.
     *
     * @return The name of the instance.
     */
    abstract protected function DoGetName();

    /**
     * Perform the export-operation on the given literature data.
     *
     * @param $literature The literature to export.
     */
    public function Export($literature)
    {
        return $this->DoExport($literature);
    }

    /**
     * Retrieves the name of the current Exporter instance.
     *
     * @return The name of the exporter.
     */
    public function GetName()
    {
        return $this->DoGetName();
    }

    /**
     * Retrieves the file extention of the current Exporter instance, or NULL if there is none.
     *
     * @return The file extention of the current Exporter instance, or NULL if there is none.
     */
    public function GetFileExtention()
    {
        return NULL;
    }

    /**
     * Retrieves the MIME-type of the current Exporter instance, or NULL if there is none.
     */
    public function GetMimeType()
    {
        return NULL;
    }
}

/**
 * A class that implements literature export in Microsoft Excel format.
 */
class SpreadsheetExporter extends LiteratureExporter
{
    /**
     * An undefined Format.
     */
    const FORMAT_UNDEFINED = 0x0000;

    /**
     * Excel Binary Format. (*.xls)
     */
    const FORMAT_BIFF = 0x0001;

    /**
     * Office OpenXML Format. (*.xlsx)
     */
    const FORMAT_OPENXML = 0x0002;

    /**
     * OASIS OpenDocument Spreadsheet Format. (*.ods)
     */
    const FORMAT_OASIS_OPENDOCUMENT = 0x0003;

    // Aliases for convenient usage
    const FORMAT_XLS = self::FORMAT_BIFF;
    const FORMAT_XLSX = self::FORMAT_OPENXML;
    const FORMAT_ODS = self::FORMAT_OASIS_OPENDOCUMENT;

    /**
     * Instantiate the SpreadsheetExporter.
     *
     * @param $type The type of the export file. Can either be SpreadsheetExporter::FORMAT_BIFF for binary excel files (*.xls; Excel 2003 or older), SpreadsheetExporter::FORMAT_OPENXML for modern excel files in OpenXML format (*.xlsx; Excel 2007 or newer), SpreadsheetExporter::FORMAT_OASIS_OPENDOCUMENT for OASIS OpenDocument Speadsheet format (*.ods; LibreOffice).
     */
    public function __construct($type = self::FORMAT_OPENXML)
    {
        parent::__construct();

        if ($type != self::FORMAT_BIFF && $type != self::FORMAT_OPENXML && $type != self::FORMAT_OASIS_OPENDOCUMENT) {
            throw new Exception("Invalid Spreadsheet Format", 1);
        }

        $this->type = $type;
    }

    /**
     * Creates a new Excel sheet.
     *
     * @return A newly instantiated PHPExcel object.
     */
    protected function CreateSheet()
    {
        require_once(realpath(dirname(__FILE__) . '/' . PHP_EXCEL_PHP_PATH_REL));
        $excel = new PHPExcel();

        return $excel;
    }

    /**
     * Sets the properties of the sheet, such as Creator, Title and Description.
     *
     * @param The Excel sheet to set the properties of.
     */
    protected function SetSheetProperties($sheet)
    {
        $p = $sheet->getProperties();

        $date = (new DateTime())->format('Y-m-d H:i:s');

        $p->setCreator('SFB 1002 Literature Repository');
        $p->setTitle('Literature Export ' . $date);
        $p->setDescription('Export of the SFB 1002 Literature Repository. (' . $date . ')');
    }

    private static function sheetExtendLiteratureDataByExternalIDs($active_sheet, $literature_ids, $header_col){

        //initialize loop
        $max = 0;
        $current_row = 2;

        //loop for entering external literature data
        foreach ($literature_ids as $id){

            $expub_list = PublicationExternalIdentifier::fetchAllByPublicationId($id);

            //
            if ($expub_list and !is_array($expub_list)) $expub_list = [$expub_list];
            if(!$expub_list) continue;

            $expub_size = count($expub_list);
            $current_col = $header_col;

            //skip if none to be added
            if(!$expub_size) continue;

            //determine maximum size for heading and possible further linking
            $max = ($expub_size > $max) ? $expub_size : $max;

            //sort external literature list by Type Label
            usort($expub_list, function ($a, $b){return strcmp($a->getTypeLabel(), $b->getTypeLabel()); } );

            foreach ($expub_list as $publication) {
                $current_cell = $active_sheet->getCell($current_col++.$current_row);

                //set cell value and clickable URL
                $current_cell->setValue($publication->getTypeLabel().": ".$publication->url('resolve'));
                $current_cell->getHyperLink()->setUrl($publication->url('resolve'));
            }
            $current_row++;
        }

        $current_col = $header_col;
        for($i=1; $i<$max; $i++) $current_col++;
        $active_sheet->setCellValue($header_col.'1', "External Resources");
        $active_sheet->mergeCells($header_col.'1:'.$current_col.'1');

        $current_col++;
        return $current_col;

    }

    private static function sheetExtendLiteratureDataByAntibodies($active_sheet, $literature_ids, $header_col){

        $max = 0;
        $current_row = 2;

        $antibody_link = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');

        foreach($literature_ids as $id){

            //get antibody IDs linked to publication
            $antibody_list = $antibody_link->getLinkings('pubreg_articles', $id);
            //overwrite IDs with actual antibody objects
            foreach ($antibody_list as $key => $value) $antibody_list[$key] = AntibodiesRepository::findById($value);

            $antibody_size = count($antibody_list);
            $current_col = $header_col;

            if(!$antibody_size) continue;

            $max = ($antibody_size > $max) ? $antibody_size : $max;

            usort($antibody_list, function ($a, $b){return strcmp($a->getType(), $b->getType());});
            foreach ($antibody_list as $antibody){
                $current_cell = $active_sheet->getCell($current_col++.$current_row);
                $current_cell->setValue($antibody->getType().": ".$antibody->getName());
            }
            $current_row++;
        }

        $current_col = $header_col;
        for($i=1; $i<$max; $i++) $current_col++;
        $active_sheet->setCellValue($header_col.'1', "Linked Antibodies");
        $active_sheet->mergeCells($header_col.'1:'.$current_col.'1');

        $current_col++;
        return $current_col;
    }

    private static function sheetExtendLiteratureDataByMouselines($active_sheet, $literature_ids, $header_col){

        $max = 0;
        $current_row = 2;

        $mouseline_link = RDPLinking::getLinkingByName('sfb_mouseline', 'mouselines_mouseline');

        foreach ($literature_ids as $id){
            $mouseline_list = $mouseline_link->getLinkings('pubreg_articles', $id);
            foreach ($mouseline_list as $key => $value) $mouseline_list[$key] = MouselineRepository::findById($value);
            $mouseline_size = count($mouseline_list);
            $current_col = $header_col;

            if(!$mouseline_size) continue;

            $max = ($mouseline_size > $max) ? $mouseline_size : $max;

            foreach ($mouseline_list as $mouseline){
                $current_cell = $active_sheet->getCell($current_col++.$current_row);
                $current_cell->setValue($mouseline->getName());
            }
            $current_row++;
        }

        $current_col = $header_col;
        for($i=1; $i<$max; $i++) $current_col++;
        $active_sheet->setCellValue($header_col.'1', "Linked Mouselines");
        $active_sheet->mergeCells($header_col.'1:'.$current_col.'1');

        $current_col++;
        return $current_col;
    }

    private static function sheetExtendLiteratureDataByCellLines($active_sheet, $literature_ids, $header_col){

        $max = 0;
        $current_row = 2;

        $cellmodel_link = RDPLinking::getLinkingByName('rdp_cellmodel', 'cellmodel_cell_line');

        foreach ($literature_ids as $id){
            $cell_line_list = $cellmodel_link->getLinkings('pubreg_articles', $id);
            foreach ($cell_line_list as $key => $value) $cell_line_list[$key] = CellLineRepository::findById($value);
            $cell_line_size = count($cell_line_list);
            $current_col = $header_col;

            if(!$cell_line_size) continue;

            $max = ($cell_line_size > $max) ? $cell_line_size : $max;

            foreach ($cell_line_list as $cell_line){
                $current_cell = $active_sheet->getCell($current_col++.$current_row);
                $current_cell->setValue("Project: ".$cell_line->getProject().", Lab ID: ".$cell_line->getLabId());
            }
            $current_row++;
        }

        $current_col = $header_col;
        for($i=1; $i<$max; $i++) $current_col++;
        $active_sheet->setCellValue($header_col.'1', "Linked Cell Lines");
        $active_sheet->mergeCells($header_col.'1:'.$current_col.'1');

        $current_col++;
        return $current_col;
    }

    /**
     * Adds literature data to sheet.
     * Adds data about linked external publications, antibodies, mouselines and cell lines to respective publications in the sheet.
     * @param $sheet PHPExcel references an empty PHPExcel sheet
     * @param $literature Array associative array obtained by querying and fetching 'pubreg_articles' table
     */
    protected function SheetAddData($sheet, $literature){

        // fields to be exported from table 'pubreg_articles' with corresponding names are saved in $fields and $names
        $fields = Publication::get_form_fields();
        $fields['creation_time'] = -1;
        $names = Publication::get_field_names();
        $names['creation_time'] = 'Creation Time';

        //initialization of variables used for writing in the sheet
        $active_sheet = $sheet->getActiveSheet();
        $current_cell = $active_sheet->getCell('A1');
        $current_col = $current_cell->getColumn();
        $current_row = $current_cell->getRow();
        $header_col = $current_cell->getColumn();

        //entering headings 'RDP - Link' and from class Publication
        $current_cell->setValue('RDP - Link');
        $current_col++;
        foreach ($fields as $head => $val){
            $current_cell = $active_sheet->getCell($current_col++.$current_row);
            $current_cell->setValue(arrayValueOrDefault($names, $head, $head));
            $header_col++;
        }
        $header_col++;

        //exit if no data is exported
        if(count($literature) <= 0) return 0;

        //base URL for links
        global $base_url;

        //used for extending literature data by linked tables (mouselines, antibodies, cell lines)
        $selected_ids = array();

        //data from table pubreg_articles is added via fields from class Publication
        foreach ($literature as $publication){

            //the IDs of the exported publications are being saved here in the order in which they are exported
            array_push($selected_ids, $publication->id);

            $current_row++;
            $current_cell = $active_sheet->getCell('A'. $current_row);
            $current_col = $current_cell->getColumn();

            //add publication registry link in first column
            $url = $base_url.'/literature/publications/'.$publication->id;
            $current_cell->getHyperlink()->setURL($url);
            $current_cell->setValue($url);
            $current_col++;

            $publication_types = Publication::publication_types();
            foreach ($fields as $key => $val){
                switch ($key){
                    case 'publication_type':
                        $data = $publication_types[$publication->publication_type];
                        break;
                    case 'open_access':
                        $data = ($publication->open_access == 0) ? 'No' : (($publication->open_access == 1) ? 'Yes' : 'Unknown');
                        break;
                    default:
                        $data = $publication->$key;
                }
                $current_cell = $active_sheet->getCell($current_col++.$current_row);
                $current_cell->setValue($data);
            }
        }

        //Attach linked module tables
        if (module_exists('rdp_external_id'))
            $header_col = self::sheetExtendLiteratureDataByExternalIDs($active_sheet, $selected_ids, $header_col);

        if (module_exists('sfb_antibody'))
            $header_col = self::sheetExtendLiteratureDataByAntibodies($active_sheet, $selected_ids, $header_col);

        if (module_exists('sfb_mouseline'))
            $header_col = self::sheetExtendLiteratureDataByMouselines($active_sheet, $selected_ids, $header_col);

        if (module_exists('rdp_cellmodel'))
            $header_col = self::sheetExtendLiteratureDataByCellLines($active_sheet, $selected_ids, $header_col);

        //Formatting for better readability and capping col_width to 80,
        //should be faster to do this in two loops
        $top_cols = $active_sheet->getCell('A1')->getColumn();
        do{
            $active_sheet->getColumnDimension($top_cols)->setAutoSize(true);
        }while($top_cols++ !== $header_col);
        $active_sheet->calculateColumnWidths();
        $top_cols = $active_sheet->getCell('A1')->getColumn();
        do{
            $dim = $active_sheet->getColumnDimension($top_cols);
            if($dim->getWidth() > 80) {
                $dim->setAutoSize(false);
                $dim->setWidth(80);
            }
        }while($top_cols++ !== $header_col);
        $active_sheet->getStyle("A1:".$header_col."1")->getFont()->setBold(true);

    }

    protected function DoExport($literature)
    {
        $sheet = $this->CreateSheet();
        $this->SetSheetProperties($sheet);
        $this->SheetAddData($sheet, $literature);

        $k = 'Excel2007';

        switch ($this->type) {
            case self::FORMAT_BIFF:
                $k = 'Excel5';
                break;
            case self::FORMAT_OPENXML:
                $k = 'Excel2007';
                break;
            case self::FORMAT_OASIS_OPENDOCUMENT:
                $k = 'OOCalc';
                break;
        }

        // This is stupid because outputting directly won't enable us to set the Content-Length header D:
        $objWriter = PHPExcel_IOFactory::createWriter($sheet, $k);
        $objWriter->save('php://output');
    }

    protected function DoGetName()
    {
        switch ($this->type) {
            case self::FORMAT_BIFF:
                return 'Excel (Binary Compatible)';
            case self::FORMAT_OPENXML:
                return 'Excel (OpenXML)';
            case self::FORMAT_OASIS_OPENDOCUMENT:
                return 'LibreOffice Calc (OASIS OpenDocument)';
        }
    }

    public function GetFileExtention()
    {
        switch ($this->type) {
            case self::FORMAT_BIFF:
                return 'xls';
            case self::FORMAT_OPENXML:
                return 'xlsx';
            case self::FORMAT_OASIS_OPENDOCUMENT:
                return 'ods';
        }
    }

    public function GetMimeType()
    {
        switch ($this->type) {
            case self::FORMAT_BIFF:
                return 'application/vnd.ms-excel';
            case self::FORMAT_OPENXML:
                return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            case self::FORMAT_OASIS_OPENDOCUMENT:
                return 'application/vnd.oasis.opendocument.spreadsheet';
        }
    }

}
