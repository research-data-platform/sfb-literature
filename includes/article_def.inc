<?php

/**
 * @deprecated Replace with PublicationForm::get_form_field('wg')
 *
 * @return array
 */
function get_form_field_working_group() {
  return PublicationForm::get_form_field('wg');
}

/**
 * @deprecated Replace with PublicationForm::get_form_field('subproject')
 *
 * @return array
 */
function get_form_field_subproject() {
  return PublicationForm::get_form_field('subproject');
}

/**
 * @deprecated Replace with PublicationForm::get_form_field('open_access')
 *
 * @return array
 */
function get_form_field_open_access() {
  return PublicationForm::get_form_field('open_access');
}

/**
 * @deprecated Replace with PublicationForm::get_form_field('publication_type')
 *
 * @return array
 */
function get_form_field_publication_type() {
  return PublicationForm::get_form_field('publication_type');
}

/**
 * @deprecated Replace with Publication::get_field_length_restrictions()
 *
 * @return array
 */
function article_get_length_restrictions() {
  $lr = Publication::get_field_length_restrictions();

  return $lr;
}

/**
 * @deprecated ToDo: Replace with OOP methods.
 *
 * @return array An associative array of WorkingGroups.
 */
function wg_get_names() {

  $groups = WorkingGroupRepository::findAll();

  $data = [];

  foreach ($groups as $group) {
    $data[$group->getShortName()] = $group->getName();
  }

  return $data;
}