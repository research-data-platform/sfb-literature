<?php

function sfb_literature_install() {
  // Automatically create module specific roles and permissions.
  create_initial_roles();

  drupal_set_message("Installed module sfb-literature.");
}

/**
 * Implements @see hook_schema()
 */
function sfb_literature_schema() {

  $schema['pubreg_articles'] = [
    'description' => 'SFB1002 Publication Registry - Research Articles',
    'fields' => [
      'id' => [
        'description' => 'Internal ID of the publication item',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'pmid' => [
        'description' => 'PubMed ID',
        'type' => 'int',
        'not null' => FALSE,
      ],
      'doi' => [
        'description' => 'Digital Object Identifier (DOI)',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
      ],
      'publication_year' => [
        'description' => 'Year of publication',
        'type' => 'int',
        'size' => 'small',
        'not null' => FALSE,
        'default' => 0,
      ],
      'title' => [
        'description' => 'Title of the publication',
        'type' => 'text',
      ],
      'journal_title' => [
        'description' => 'Name of the journal that published the article',
        'type' => 'text',
      ],
      'issn' => [
        'description' => 'ISSN number of the journal',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'essn' => [
        'description' => 'ESSN number of the journal',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'url' => [
        'description' => 'Weblink to original publication web page',
        'type' => 'text',
      ],
      'pages' => [
        'description' => 'Number or range of pages of the article',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'issue' => [
        'description' => 'Issue of the journal',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'volume' => [
        'description' => 'Volume of the journal',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ],
      'journal_abbr' => [
        'description' => 'Abbreviation of the journal name',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'extra' => [
        'description' => 'Additional information',
        'type' => 'text',
      ],
      'publication_type' => [
        'description' => 'Type of the publicaton (article, conference abstract, etc.)',
        'type' => 'varchar',
        'length' => '128',
        'not null' => TRUE,
        'default' => 'unknown',
      ],
      'authors' => [
        'description' => 'Authors of the publication',
        'type' => 'text',
      ],
      'first_author' => [
        'description' => 'First ranked author of the publication',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'last_author' => [
        'description' => 'Senior author of the publication',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
        'default' => NULL,
      ],
      'antibody_info' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'gene_prot' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'animals' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'cell_lines' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'links' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'protocols' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'wg' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'text',
      ],
      'subproject' => [
        'description' => 'Subproject in which the publication was written',
        'type' => 'text',
      ],
      'open_access' => [
        'description' => 'TODO: please describe this field!',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
        'default' => 0,
      ],
      'creation_time' => [
        'description' => 'TODO: please describe this field!',
        'mysql_type' => 'timestamp',
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['id'],
  ];

  $schema['rdp_publication_externalid'] = [
    'description' => 'Links External-ID items to a description text',
    'fields' => [
      'external_id' => [
        'description' => 'External ID',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'description' => [
        'description' => 'Description of the External-ID.',
        'type' => 'varchar',
        'length' => '2040',
        'not null' => FALSE,
      ],
      'order_position' => [
        'description' => 'Order of the External ID.',
        'type' => 'int',
      ],
    ],
    'primary key' => ['external_id'],
  ];

  return $schema;
}

/**
 * Update table pubreg_articles, field publication_type:
 * Publication type NULL is not allowed anymore, default value is changed to
 * 'unknown'. Update all existing NULL values to 'unknown'.
 */
function sfb_literature_update_7002() {
  $table = 'pubreg_articles';
  $field = 'publication_type';

  $db_or = db_or()
    ->isNull($field)
    ->condition($field, '');

  db_update($table)
    ->fields([$field => 'unknown'])
    ->condition($db_or)
    ->execute();

  db_change_field('pubreg_articles', 'publication_type', 'publication_type', [
    'description' => 'TODO: please describe this field!',
    'type' => 'varchar',
    'length' => '128',
    'not null' => TRUE,
    'default' => 'unknown',
  ]);
}

/**
 * Update table pubreg_articles, fields 'pmid' and 'doi':
 * Remove default values.
 */
function sfb_literature_update_7003() {
  $table = 'pubreg_articles';
  $fields = ['pmid', 'doi'];
  $spec = [
    'pmid' => [
      'description' => 'PubMed ID',
      'type' => 'int',
      'not null' => FALSE,
    ],
    'doi' => [
      'description' => 'Digital Object Identifier (DOI)',
      'type' => 'varchar',
      'length' => '128',
      'not null' => FALSE,
    ],
  ];
  foreach ($fields as $field) {
    db_change_field($table, $field, $field, $spec[$field]);
  }
}

/**
 *  Update table pubreg_articles, field 'issue':
 *  default value to NULL
 *  Update all rows where field is 'NULL' or empty
 */
function sfb_literature_update_7004() {
  $table = 'pubreg_articles';
  $field = 'issue';
  $spec = [
    'issue' => [
      'default' => NULL,
      'type' => 'varchar',
      'length' => '128',
      'not null' => FALSE,
    ],
  ];
  db_change_field($table, $field, $field, $spec[$field]);

  // update all rows where field is empty or a string containing NULL
  $db_or = db_or()
    ->condition($field, '')
    ->condition($field, 'NULL');

  db_update($table)
    ->fields([$field => NULL])
    ->condition($db_or)
    ->execute();
}

/**
 * External ID initiation
 */
function sfb_literature_update_7005() {
  if (!module_exists("rdp_external_id")) {
    try {
      module_enable(["rdp_external_id"], TRUE);
    } catch (Exception $e) {
      drupal_set_message($e->getMessage(), "error");
    }
  }

  $table = "rdp_publication_externalid";

  if (!db_table_exists($table)) {
    $spec = [
      'description' => 'Links External-ID items to a description text',
      'fields' => [
        'external_id' => [
          'description' => 'External ID',
          'type' => 'int',
          'not null' => TRUE,
        ],
        'description' => [
          'description' => 'Description of the External-ID.',
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ],
      ],
      'primary key' => ['external_id'],
    ];

    db_create_table($table, $spec);
  }
}

/**
 * Increase maximum length of External-ID description
 */
function sfb_literature_update_7006() {
  $table = "rdp_publication_externalid";
  $field = 'description';
  $spec = [
    'description' => 'Description of the External-ID.',
    'type' => 'varchar',
    'length' => '2040',
    'not null' => FALSE,
  ];
  db_change_field($table, $field, $field, $spec);
}

/**
 * Add ordering field for External-ID
 */
function sfb_literature_update_7008() {
  $table = "rdp_publication_externalid";
  $field = 'order_position';
  $spec = [
    'description' => 'Order position of the External-ID.',
    'type' => 'int',
  ];
  db_add_field($table, $field, $spec);
}

/**
 * Add field for subprojects
 */
function sfb_literature_update_7009() {
  $table = "pubreg_articles";
  $field = 'subproject';
  $spec = [
    'description' => 'Subproject in which the publication was written.',
    'type' => 'text',
  ];
  db_add_field($table, $field, $spec);
}